import React, { Component } from "react"
import { Text, View ,StyleSheet, Image} from 'react-native'
import { TouchableOpacity } from "react-native-gesture-handler"
import { DrawerActions } from 'react-navigation-drawer';
import { NavigationActions, StackActions } from "react-navigation";

       const Header=({navigation})=>{
           return(    
               <View style={{
                height:"7%",
                width:"100%",
                flexDirection:"row",
                backgroundColor:"white",
                borderTopLeftRadius:20,
                borderTopRightRadius:20,
                alignItems:"center"
                }}>
                    <TouchableOpacity onPress={()=>navigation.dispatch(DrawerActions.toggleDrawer())}>
                 <Image source={require("../assets/menu.png")} style={{marginLeft:"3%"}}/>
                 </TouchableOpacity>
                 <Text style={{color:"green",fontSize:28,fontWeight:"bold",marginLeft:"12%"}}>MP</Text>
                 <Text style={{fontSize:20,fontWeight:"bold",marginLeft:"3%"}}>MatchPlay!</Text>
                 <Image source={require("../assets/notification.png")} style={{height:22,width:22,marginLeft:"13%"}}/>
                 <Image source={require("../assets/wallet.png")}  style={{height:20,width:20,marginLeft:"7%"}}/>
                </View>
            
        )
}
export default Header;
