import React, { Component } from "react"
import { ImageBackground, Text, View ,StyleSheet, Image} from 'react-native'
import { TextInput,TouchableOpacity} from "react-native-gesture-handler"
import LinearGradient from "react-native-linear-gradient"

export default class otpVerify extends Component{
    constructor(props){
        super(props)
        this.state={
            otp1:"",otp2:"",otp3:"",otp4:""
        }
    }
    render(){
        return(
            <View>
                <ImageBackground source={require("../../assets/background.png")} style={{height:"100%",width:"100%"}}>
                <Image source={require("../../assets/MP.png")} style={styles.MP_Image}/>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>
               <Text style={styles.MatchPlay}>MatchPlay!</Text>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>
               <Text style={{color:"white",alignSelf:"center",fontSize:18,marginTop:"5%"}}>Enter OTP</Text>
               <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",width:"70%"}}>Pleas enter OTP to change your password</Text>
               <View style={styles.mainView}>
                <TextInput  style={styles.otpTextInput}
                maxLength={1}
                keyboardType={"numeric"}
                ref="input1"
                onChangeText={otp1 => {
                    this.setState({ otp1 })
                    if (otp1) this.refs.input2.focus();
                }}
                />
                 <TextInput  style={styles.otpTextInput}
                maxLength={1}
                keyboardType={"numeric"}
                ref="input2"
                onChangeText={otp2 => {
                    this.setState({ otp2 })
                    if (otp2) this.refs.input3.focus();
                }}
                />
                 <TextInput  style={styles.otpTextInput}
                maxLength={1}
                keyboardType={"numeric"}
                ref="input3"
                onChangeText={otp3 => {
                    this.setState({ otp3 })
                    if (otp3) this.refs.input4.focus();
                }}
                />
                 <TextInput  style={styles.otpTextInput}
                maxLength={1}
                keyboardType={"numeric"}
                ref="input4"
                onChangeText={otp4 => {
                    this.setState({ otp4 })
                   
                }}
                />
               </View>
               <View style={{top:'10%'}}>
               <TouchableOpacity onPress={()=>this.props.navigation.navigate("setPassword")}
                    style={{top:'10%'}}
               >
             
               <LinearGradient  start={{x:0,y:0}}
                end={{x:1,y:0}}
                locations={[0,0.4,0.6]}
                colors={["rgba(164, 166, 60, 1)", "#D4CE47", "rgba(97, 142, 45, 0.8)"]} 
                style={{borderRadius:40,height:45}}
               >
                   <Text style={{color:"white",fontSize:20,alignSelf:"center"}}>SUBMIT</Text>
                   <Image source={require("../../assets/arrow.png")} style={{marginLeft:"70%",marginTop:'-8%',height:25,width:25}}/>
                     </LinearGradient> 
             
               </TouchableOpacity>
               </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles=StyleSheet.create({
  
    MP_Image:{
        marginTop:"20%",
        alignSelf:'center'
    },
    rectangle1:{
        alignSelf:'center'
    },
    mainView:{
        width:"80%",
        backgroundColor:"rgba(255,255,255,0.4)",
        marginLeft:"10%",
        borderRadius:40,
        marginTop:"10%",
        flexDirection:"row",
        justifyContent:"space-evenly",
        alignItems:"center"
    },
    otpTextInput:{
        borderBottomWidth:1,
        borderBottomColor:"white",
        alignSelf:"center",
        justifyContent:"center",
        marginBottom:"3%",
    
    },
 
    MatchPlay:{
        color:'white',
        marginLeft:'33%',
        fontSize:20
    },
    rectangle2:{
        alignSelf:"center",
        marginTop:"2%"
    },
    user:{
        height:25,
        width:25,
       resizeMode:"stretch",
       marginLeft:"5%",
       padding:10
    },
    subView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
      
        marginTop:"10%",
        alignItems:"center"
    },
    phoneView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
        marginTop:"3%",
        alignItems:"center"
    },
    forgot_password:{
        color:'white',
        fontSize:16,
        marginLeft:"55%",
        marginTop:'3%'
     },
     
 })
 