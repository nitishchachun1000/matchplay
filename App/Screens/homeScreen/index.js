import React, {} from "react"
import { ImageBackground, Text, View ,StyleSheet, Image} from 'react-native'
import Swiper from "react-native-swiper"
import Header from '../../component/Header'
import {ScrollView} from 'react-native-gesture-handler'

const homeScreen=({navigation})=>{
        return(
            <View style={{flex:1}}>
            <ImageBackground source={require("../../assets/background.png")} style={{height:"100%",width:"100%"}}>
               <Header  navigation={navigation}/>
               <ScrollView>
                   <View style={{height:"80%",width:"100%",marginBottom:"10%"}}>
                   <View style={{height:200,width:360}} >
                   <Swiper autoplay={false} >
               <Image source={require("../../assets/playGolf.png")}/>
               <Image source={require("../../assets/golf3.jpg")} style={styles.Image_style}/>
               <Image source={require("../../assets/golf2.png")} style={styles.Image_style}/>
               <Image source={require("../../assets/golf1.png")}  style={styles.Image_style}/>
               </Swiper>
               </View>
               <View style={{flexDirection:"row",marginTop:"3%",height:"15%"}}>
               <Image source={require("../../assets/alex.png")}/>
               <View style={{flexDirection:"column",marginLeft:"5%",marginTop:"3%"}}>
                   <Text style={{color:"white"}}>ALEX SANDROS</Text>
                   <Text  style={{color:"white"}}>share match results</Text>
                    </View>
                    <Text style={{color:"white",marginLeft:"15%",marginTop:"5%"}}>6:30 PM</Text>
               </View>
               <View style={styles.subView}>
               <Text style={{color:"white",alignSelf:"center"}}>ALEX SANDROS WINS THE MATCH</Text>
               <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:"2%",marginLeft:"2%"}}>
               <View style={{flexDirection:"column"}}>
                   <Image source={require("../../assets/alex.png")}/>
                   <Text style={{color:"white"}}>ALEX SANDROS</Text>
                   <Image source={require("../../assets/germony_flag.png")}/>
                   <Text style={{color:"white",marginTop:"-12%",marginLeft:"12%"}}>Germony</Text>
               </View>
               <View style={{flexDirection:"column"}}>
                 <Text style={{color:"white",fontSize:20,marginLeft:"17%"}}>23:18</Text>
                 <Text style={{color:"white",fontSize:12}}>18 HOLE MATCH</Text>
                 <Text style={{color:"white",fontSize:12}}>At Lakeside Golf Club</Text>
                 <Text style={{color:"white",fontSize:12,marginLeft:"10%"}}>Tournament:No</Text>
               </View>
               <View style={{flexDirection:"column"}}>
                   <Image source={require("../../assets/tommy.png")}/>
                   <Text style={{color:"white"}}>TOMMY JR</Text>
                   <Image source={require("../../assets/germony_flag.png")}/>
                   <Text style={{color:"white",marginTop:"-12%",marginLeft:"12%"}}>Germony</Text>
               </View>
               </View>
               </View>
               <View style={{flexDirection:"row",marginTop:"5%"}}>
                <Image source={require("../../assets/heart.png")} style={{height:20,width:20,marginLeft:"7%"}}/>
                <Text style={{color:"white"}}>40k</Text>
                <Image source={require("../../assets/comment.png")} style={{height:20,width:20,marginLeft:"7%"}}/>
                <Text style={{color:"white"}}>2200</Text>
                <Image source={require("../../assets/share.png")} style={{height:20,width:20,marginLeft:"7%"}}/>
                <Text style={{color:"white"}}>141</Text>
               </View>
               <Image source={require('../../assets/fullLine.png')} style={{width:"90%",height:1,marginLeft:"5%",marginTop:"2%"}}/>
               <View style={{marginTop:"5%"}}>
               <Image source={require("../../assets/playGolf.png")} style={{height:200,width:"100%"}}/>
               </View>
               </View>
               </ScrollView>
               <View style={{flexDirection:"row",marginTop:"3%",height:"15%"}}>
               <Image source={require("../../assets/alex.png")}/>
               <View style={{flexDirection:"column",marginLeft:"5%",marginTop:"3%"}}>
                   <Text style={{color:"white"}}>ALEX SANDROS</Text>
                   <Text  style={{color:"white"}}>share match results</Text>
                    </View>
                    <Text style={{color:"white",marginLeft:"15%",marginTop:"5%"}}>6:30 PM</Text>
               </View>
            </ImageBackground>
            </View>
        )
    }

const styles=StyleSheet.create({
   subView:{
   backgroundColor:"rgba(255,255,255,0.4)",
   width:"90%",
   height:"28%",
   marginLeft:"5%",
   borderRadius:20

   },
   Image_style:{
       height:"100%",
       width:"100%"
   }
})
export default homeScreen;