import React, { Component } from "react"
import { Image, Text, View} from 'react-native'


export default  class splash extends Component{

    componentDidMount() {
        this.timeoutHandle = setTimeout(() => {
          this.retrieveData();
        }, 1000);
        
      }
      retrieveData(){
          this.props.navigation.navigate("signIn")
      }
    render(){
     return(
           <View>
               <Image source={require("../../assets/splash.png")} style={{height:"100%",width:"100%"}}/>
             </View>
        )
    }
}