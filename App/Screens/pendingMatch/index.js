import React,{ useState } from "react"
import {View,Text,ImageBackground,Image, TouchableOpacity,StyleSheet} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import Header from '../../component/Header'
import LinearGradient from "react-native-linear-gradient"

    const pendingMatch=({navigation})=>{
    const [golfClub,setGolfClub]= useState("CHOOSE A GOLF CLUB") 
    const [golfOption,showGolfOption]= useState(true)
         const[timeSlot,setTimeSlot]=useState(true)
         const[timing,setTiming]=useState("CHOOSE TIME SLOT")
         const [bett,setBett]=useState(true)
         const [betttype,setbettType]=useState("BETTING")
        const [show ,setShow]=useState(true)
         const [Match,setMatch]=useState("MATCH FORMAT")
         const [show1,setShow1]=useState(true)
         const [balltype,setBallType]=useState("MATCH TYPE")
         const [showData,setShowData]=useState(false)
        
         const golfShowFn=(f)=>{
            showGolfOption(f)
         }
         
         const selectClubFn=(g)=>{
             setGolfClub(g)
             showGolfOption(true)
         }

       const timeSlotFn=(b)=>{
       setTimeSlot(b)
        }
        const timeOutFn=(c)=>{
            setTiming(c)
            setTimeSlot(true)
        }
       const  showView=(value)=>{
             setShow(value)
         }
         const setTextValue=(value1)=>{
             setMatch(value1)
             setShow(true)
         }

         const  showView1=(value2)=>{
            setShow1(value2)
        }

       const  showBallType=(value3)=>{
            setBallType(value3)
            setShow1(true)
        }
        const bettView=(value4)=>{
            setBett(value4)
        }
        const betttypeFunc=(a)=>{
            setbettType(a)
            setBett(true)
        }
        const changeData=(e1)=>{
         setShowData(e1)
        }

               return <View style={{flex:1}}>
                <Header navigation={navigation}/>
                <ImageBackground source={require("../../assets/background.png")} style={{flex:1}}>
                  
                   
                    <View style={{marginTop:"10%",flexDirection:"row"}}>
                        <TouchableOpacity onPress={()=>{changeData(true)}}>
                        <View style={styles.subView}>
                    
                          <Text style={{fontWeight:"bold",color:showData==false?"black":"green"}}>FIND A GAME</Text>
                        </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{changeData(false)}}>
                        <View style={styles.subView1}>
                          <Text style={{fontWeight:"bold"}}>CHALLENGE</Text>
                        </View>
                        </TouchableOpacity>
                    </View>
                    {showData==true?
                    <View>
                         <ScrollView style={{}}>
                    {golfOption===true?
                    
                <View style={{height:45,width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20,flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
                <Image source={require("../../assets/timer.png")} style={{height:25,width:20}}/>
                 <Text style={{color:"white",fontSize:18}}>{golfClub}</Text>
                 <TouchableOpacity onPress={()=>{golfShowFn(false)}}>
                  <Image source={require("../../assets/bottomIcon.png")} style={{height:15,width:15}}/>
                 </TouchableOpacity>
                 </View>:
                 <View style={{width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20}}>
                  <View style={{flexDirection:"row",alignItems:"center",marginTop:"3.5%",justifyContent:"space-evenly"}}>
                  <Image source={require("../../assets/timer.png")} style={{height:25,width:20}}/>
                   <Text style={{color:"white",fontSize:18}}>{betttype}</Text>
                   <TouchableOpacity  onPress={()=>{golfShowFn(true)}}>
                    <Image source={require("../../assets/upIcon.png")} style={{height:15,width:15}}/>
                   </TouchableOpacity>
                   </View>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                  <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%"}}
                  onPress={()=>{selectClubFn("DEHRADUN GOLF CLUB")}}
                  >DEHRADUN GOLF CLUB</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{selectClubFn("CHANDIGARH GOLF CLUB")}}
                   >CHANDIGARH GOLF CLUB</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{selectClubFn("DELHI GOLF CLUB")}}
                   >DELHI GOLF CLUB</Text>
                   </View>}

                    {timeSlot===true?
                <View style={{height:45,width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20,flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
                <Image source={require("../../assets/timer.png")} style={{height:25,width:20}}/>
                 <Text style={{color:"white",fontSize:18}}>{timing}</Text>
                 <TouchableOpacity onPress={()=>{timeSlotFn(false)}}>
                  <Image source={require("../../assets/bottomIcon.png")} style={{height:15,width:15}}/>
                 </TouchableOpacity>
                 </View>:
                 <View style={{width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20}}>
                  <View style={{flexDirection:"row",alignItems:"center",marginTop:"3.5%",justifyContent:"space-evenly"}}>
                  <Image source={require("../../assets/timer.png")} style={{height:25,width:20}}/>
                   <Text style={{color:"white",fontSize:18}}>{betttype}</Text>
                   <TouchableOpacity  onPress={()=>{timeSlotFn(true)}}>
                    <Image source={require("../../assets/upIcon.png")} style={{height:15,width:15}}/>
                   </TouchableOpacity>
                   </View>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                  <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%"}}
                  onPress={()=>{timeOutFn("9:00-9:30")}}
                  >9:00-9:30</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{timeOutFn(" 10:30:11:00")}}
                   >10:30:11:00</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{timeOutFn("12:00:12:30")}}
                   >12:00:12:30</Text>
                   </View>}


                    {bett===true?
                <View style={{height:45,width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20,flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
                <Image source={require("../../assets/coin1.png")} style={{height:25,width:20}}/>
                 <Text style={{color:"white",fontSize:18}}>{betttype}</Text>
                 <TouchableOpacity onPress={()=>{bettView(false)}}>
                  <Image source={require("../../assets/bottomIcon.png")} style={{height:15,width:15}}/>
                 </TouchableOpacity>
                 </View>:
                 <View style={{width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20}}>
                  <View style={{flexDirection:"row",alignItems:"center",marginTop:"3.5%",justifyContent:"space-evenly"}}>
                  <Image source={require("../../assets/coin1.png")} style={{height:25,width:20}}/>
                   <Text style={{color:"white",fontSize:18}}>{betttype}</Text>
                   <TouchableOpacity  onPress={()=>{bettView(true)}}>
                    <Image source={require("../../assets/upIcon.png")} style={{height:15,width:15}}/>
                   </TouchableOpacity>
                   </View>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                  <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%"}}
                  onPress={()=>{betttypeFunc("10 $")}}
                  >10 $</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{betttypeFunc("50 $")}}
                   >50 $</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{betttypeFunc("100 $")}}
                   >100 $</Text>
                   </View>}

                {show===true? 
                <View style={{height:45,width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20,flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
                <Image source={require("../../assets/player.png")} style={{height:25,width:20}}/>
                 <Text style={{color:"white",fontSize:18}}>{Match}</Text>
                 <TouchableOpacity onPress={()=>{showView(false)}}>
                  <Image source={require("../../assets/bottomIcon.png")} style={{height:15,width:15}}/>
                 </TouchableOpacity>
                 </View>:
                 <View style={{width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20}}>
                  <View style={{flexDirection:"row",alignItems:"center",marginTop:"3.5%",justifyContent:"space-evenly"}}>
                  <Image source={require("../../assets/player.png")} style={{height:25,width:20}}/>
                   <Text style={{color:"white",fontSize:18}}>{Match}</Text>
                   <TouchableOpacity onPress={()=>{showView(true)}}>
                    <Image source={require("../../assets/upIcon.png")} style={{height:15,width:15}}/>
                   </TouchableOpacity>
                   </View>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                  
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%"}} 
                   onPress={(value1)=>{setTextValue("STROKEPLAY")}}>STROKEPLAY</Text>
                  
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                   onPress={(value1)=>{setTextValue("STABLEFORD")}}
                   >STABLEFORD</Text>
                   </View>
                   }
                 {show1===true?
                <View style={{height:45,width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20,flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
                <Image source={require("../../assets/flag.png")} style={{height:25,width:20}}/>
                 <Text style={{color:"white",fontSize:18}}>{balltype}</Text>
                 <TouchableOpacity onPress={()=>{showView1(false)}}>
                  <Image source={require("../../assets/bottomIcon.png")} style={{height:15,width:15}}/>
                 </TouchableOpacity>
                 </View>:
                 <View style={{width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20}}>
                  <View style={{flexDirection:"row",alignItems:"center",marginTop:"3.5%",justifyContent:"space-evenly"}}>
                  <Image source={require("../../assets/flag.png")} style={{height:25,width:20}}/>
                   <Text style={{color:"white",fontSize:18}}>{balltype}</Text>
                   <TouchableOpacity  onPress={()=>{showView1(true)}}>
                    <Image source={require("../../assets/upIcon.png")} style={{height:15,width:15}}/>
                   </TouchableOpacity>
                   </View>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                  <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%"}}
                  onPress={()=>{showBallType("2 BALL")}}
                  >2 BALL</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{showBallType("3 BALL")}}
                   >3 BALL</Text>
                   <Image source={require("../../assets/line2.png")} style={{marginTop:"5%",width:"80%",marginLeft:"20%",height:2}}/>
                   <Text style={{color:"white",alignSelf:"center",fontSize:16,marginTop:"3%",marginBottom:"3%"}}
                    onPress={()=>{showBallType("4 BALL")}}
                   >4 BALL</Text>
                   </View>}
                   <View style={{marginTop:"13%",width:"80%",alignSelf:"center"}}>
               <LinearGradient   start={{x:0,y:0}}
                end={{x:1,y:0}}
                locations={[0,0.4,0.6]}
                colors={["rgba(164, 166, 60, 1)", "#D4CE47", "rgba(97, 142, 45, 0.8)"]} 
                style={{borderRadius:40,height:45}}
               >
                   <Text style={{color:"white",fontSize:20,alignSelf:"center",marginTop:"2%"}}>FIND GAME</Text>
                   <Image source={require("../../assets/arrow.png")} style={{marginLeft:"70%",marginTop:'-8%',height:25,width:25}}/>
                     </LinearGradient> 
               </View>
               </ScrollView>
              </View>
               :
               <View style={{height:45,width:"90%",backgroundColor:"rgba(255,255,255,0.4)",marginTop:"10%",marginLeft:"5%",borderRadius:20,flexDirection:"row",alignItems:"center",justifyContent:"space-evenly"}}>
                <TextInput 
                placeholder="SEARCH PLAYER"
                placeholderTextColor="white"
                style={{flex:1,color:"white"}}/>
                <Image source={require("../../assets/search_icon.png")} style={{height:25,width:25,marginRight:30}}/>
                </View>}
                  
                   </ImageBackground>
            </View>
        
}

    const styles=StyleSheet.create({
    subView:{
        backgroundColor:"white",
        width:165,
        height:50,
        justifyContent:"center",
        alignItems:"center",
        marginLeft:15,
        borderBottomLeftRadius:30,
        borderTopLeftRadius:30
    },
    subView1:{
        backgroundColor:"rgba(255,255,255,0.4)",
        width:165,
        height:50,
        justifyContent:"center",
        alignItems:"center",
        borderTopRightRadius:30,
        borderBottomRightRadius:30
    }
})
export default pendingMatch;