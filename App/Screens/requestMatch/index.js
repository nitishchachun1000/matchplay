import React, { Component } from "react" 
import {View,Text, ImageBackground, Image} from "react-native"
import Header from '../../component/Header'

const requestMatch=({navigation})=>{
        return(
            <View style={{flex:1}}>
                <ImageBackground source={require("../../assets/background.png")} style={{flex:1}}>
               <Header navigation={navigation}/>
               <Text style={{color:"white",alignSelf:"center",fontSize:18,marginTop:"5%"}}>FIND AND PLAY MATCH/GAME</Text>
               <Text style={{color:"white",alignSelf:"center",fontSize:18}}> WITH PLAYERS</Text>
               <View style={{height:"20%",width:"80%",backgroundColor:"rgba(255,255,255,0.41)",borderRadius:20,marginLeft:"10%",marginTop:"5%",justifyContent:"center",alignItems:"center"}}>
               <Image source={require("../../assets/nearbyPlayer.png")} style={{height:50,width:50}}/>
               <Text style={{color:"white"}}>NEARBY</Text>
               
              
               
               </View>
               <Image source={require("../../assets/forward2.png")} style={{marginLeft:"75%",marginTop:"-24%"}}/>

               <View style={{height:"20%",width:"80%",backgroundColor:"rgba(255,255,255,0.41)",borderRadius:20,marginLeft:"10%",marginTop:"27%",justifyContent:"center",alignItems:"center"}}>
               <Image source={require("../../assets/scheduled.png")} style={{height:50,width:50}}/>
               <Text style={{color:"white"}}>SCHEDULED</Text>
               
               </View>
               <Image source={require("../../assets/forward2.png")} style={{marginLeft:"75%",marginTop:"-24%"}}/>
               </ImageBackground>
            </View>
        )
    }
  export default requestMatch;