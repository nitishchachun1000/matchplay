import React, { Component } from "react"
import { ImageBackground, Text, View ,StyleSheet, Image} from 'react-native'
import { TextInput,TouchableOpacity} from "react-native-gesture-handler"
import LinearGradient from "react-native-linear-gradient"

      const forgotPassword=({navigation})=>{
            return(
               <View>
               <ImageBackground source={require("../../assets/background.png")} style={{height:"100%",width:"100%"}}>
               <Image source={require("../../assets/MP.png")} style={styles.MP_Image}/>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>
               <Text style={styles.MatchPlay}>MatchPlay!</Text>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>
               <Text style={{color:"white",fontSize:18,alignSelf:"center",marginTop:"5%"}}>Forgot Password</Text>
               <Text style={{color:"white",width:"70%",alignSelf:"center",marginTop:"3%"}}>Enter your email to receive OTP in your email to change your password</Text>

               <View style={styles.subView}>
                   <Image source={require("../../assets/email.png")} style={{height:17,width:20,marginLeft:"5%"}}/>
                  <TextInput placeholder="EMAIL" placeholderTextColor="white" color="white"
                  style={{flex:1,fontWeight:"bold"}}
                 />
               </View>
              <Text style={{color:"white",alignSelf:"center",fontSize:18,marginTop:"3%"}}>OR</Text>
              <View style={styles.phoneView}>
                   <Image source={require("../../assets/phone.png")} style={{height:17,width:20,marginLeft:"5%"}}/>
                  <TextInput placeholder="PHONE NUMBER" placeholderTextColor="white" color="white"
                  style={{flex:1,fontWeight:"bold"}}
                 />
               </View>
               <TouchableOpacity onPress={()=>navigation.navigate("otpVerify")}>
               <View style={{marginTop:"10%",width:"80%",alignSelf:"center"}}>
               <LinearGradient  start={{x:0,y:0}}
                end={{x:1,y:0}}
                locations={[0,0.4,0.6]}
                colors={["rgba(164, 166, 60, 1)", "#D4CE47", "rgba(97, 142, 45, 0.8)"]} 
                style={{borderRadius:40,height:45}}
               >
                   <Text style={{color:"white",fontSize:20,alignSelf:"center",marginTop:"2%"}}>SUBMIT</Text>
                   <Image source={require("../../assets/arrow.png")} style={{marginLeft:"70%",marginTop:'-8%',height:25,width:25}}/>
                     </LinearGradient> 
               </View>
               </TouchableOpacity>
               </ImageBackground>
            </View>
        )
    }


const styles=StyleSheet.create({
  
    MP_Image:{
        marginTop:"20%",
        alignSelf:'center'
    },
    rectangle1:{
        alignSelf:'center'
    },
 
    MatchPlay:{
        color:'white',
        marginLeft:'33%',
        fontSize:20
    },
    rectangle2:{
        alignSelf:"center",
        marginTop:"2%"
    },
    user:{
        height:25,
        width:25,
       resizeMode:"stretch",
       marginLeft:"5%",
       padding:10
    },
    subView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
      
        marginTop:"10%",
        alignItems:"center"
    },
    phoneView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
        marginTop:"3%",
        alignItems:"center"
    },
    forgot_password:{
        color:'white',
        fontSize:16,
        marginLeft:"55%",
        marginTop:'3%'
     },
     
 })
 export default forgotPassword;
 