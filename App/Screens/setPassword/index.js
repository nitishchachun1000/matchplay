import React, { Component } from "react"
import { ImageBackground, Text, View ,StyleSheet, Image,TextInput,TouchableOpacity} from 'react-native'
import LinearGradient from "react-native-linear-gradient"

const setPassword=({navigation})=>{
        return(
            <View>
              <ImageBackground source={require("../../assets/background.png")} style={{height:"100%",width:"100%"}}>
              <Image source={require("../../assets/MP.png")} style={styles.MP_Image}/>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>
               <Text style={styles.MatchPlay}>MatchPlay!</Text>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>

               <View style={styles.subView}>
                   <Image source={require("../../assets/lock.png")} style={styles.user}/>
                  <TextInput placeholder="PASSWORD" placeholderTextColor="white"
                  style={{flex:1}}
                 />
               </View>
               <View style={styles.subView}>
                   <Image source={require("../../assets/lock.png")} style={styles.user}/>
                  <TextInput placeholder=" CONFIRM PASSWORD" placeholderTextColor="white"
                  style={{flex:1}}
                 />
               </View>
               <TouchableOpacity onPress={()=>navigation.navigate("tabNavigator2")}>
               <View style={{marginTop:"13%",width:"80%",alignSelf:"center"}}>
               <LinearGradient   start={{x:0,y:0}}
                end={{x:1,y:0}}
                locations={[0,0.4,0.6]}
                colors={["rgba(164, 166, 60, 1)", "#D4CE47", "rgba(97, 142, 45, 0.8)"]} 
                style={{borderRadius:40,height:45}}
               >
                   <Text style={{color:"white",fontSize:20,alignSelf:"center",marginTop:"2%"}}>SUBMIT</Text>
                   <Image source={require("../../assets/arrow.png")} style={{marginLeft:"70%",marginTop:'-8%',height:25,width:25}}/>
                     </LinearGradient> 
               </View>
               </TouchableOpacity>
              </ImageBackground>
            </View>
        )
    }


const styles=StyleSheet.create({
  
    MP_Image:{
        marginTop:"20%",
        alignSelf:'center'
    },
    rectangle1:{
        alignSelf:'center'
    },
    mainView:{
        width:"80%",
        backgroundColor:"rgba(255,255,255,0.4)",
        marginLeft:"10%",
        borderRadius:40,
        marginTop:"10%",
        flexDirection:"row",
        justifyContent:"space-evenly",
        alignItems:"center"
    },
    otpTextInput:{
        borderBottomWidth:1,
        borderBottomColor:"white",
        alignSelf:"center",
        justifyContent:"center",
        marginBottom:"3%",
    
    },
 
    MatchPlay:{
        color:'white',
        marginLeft:'33%',
        fontSize:20
    },
    rectangle2:{
        alignSelf:"center",
        marginTop:"2%"
    },
    user:{
        height:25,
        width:25,
       resizeMode:"stretch",
       marginLeft:"5%",
       padding:10
    },
    subView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
      
        marginTop:"10%",
        alignItems:"center"
    },
    phoneView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
        marginTop:"3%",
        alignItems:"center"
    },
    forgot_password:{
        color:'white',
        fontSize:16,
        marginLeft:"55%",
        marginTop:'3%'
     },
     
 })
 export default setPassword;
 