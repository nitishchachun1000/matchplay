import React, { Component } from "react"
import { ImageBackground, Text, View,StyleSheet,Image,TextInput} from 'react-native'
import LinearGradient from "react-native-linear-gradient"


       const signup=()=>{
        return(
            <View>
                <ImageBackground source={require("../../assets/background.png")} style={{height:"100%",width:"100%"}}>
                <Image source={require("../../assets/MP.png")} style={styles.MP_Image}/>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>
               <Text style={styles.MatchPlay}>MatchPlay!</Text>
               <Image source={require('../../assets/rectangle2.png')} style={styles.rectangle2}/>

               <View style={styles.subView}>
                   <Image source={require("../../assets/email.png")} style={styles.email}/>
                  <TextInput placeholder="EMAIL" placeholderTextColor="white"
                  style={{flex:1}}
                 />
               </View>
               <View style={styles.subView}>
                   <Image source={require("../../assets/user.png")} style={styles.user}/>
                  <TextInput placeholder="USERNAME" placeholderTextColor="white"
                  style={{flex:1}}
                 />
               </View>
               <View style={styles.subView}>
                   <Image source={require("../../assets/lock.png")} style={styles.user}/>
                  <TextInput placeholder="PASSWORD" placeholderTextColor="white"
                  style={{flex:1}}
                 />
               </View>
               <View style={styles.subView}>
                   <Image source={require("../../assets/lock.png")} style={styles.user}/>
                  <TextInput placeholder=" CONFIRM PASSWORD" placeholderTextColor="white"
                  style={{flex:1}}
                 />
               </View>
               <View style={{marginTop:"10%",width:"80%",alignSelf:"center"}}>
               <LinearGradient  colors={["#A4A63C", "#D4CE47", "#618E2D"]} 
                style={{borderRadius:40,height:45}}
               >
                   <Text style={{color:"white",fontSize:20,alignSelf:"center",marginTop:"2%"}}>SIGN UP </Text>
                   <Image source={require("../../assets/arrow.png")} style={{marginLeft:"70%",marginTop:'-8%',height:20,width:20}}/>
                     </LinearGradient> 
               </View>
               <Text style={{color:"white",marginTop:"5%",marginLeft:"10%"}}>LAREADY A PLAYER! GOOD.   TEE-OFF</Text>
                </ImageBackground>
            </View>
        )
    }


const styles=StyleSheet.create({
  
    MP_Image:{
        marginTop:"20%",
        alignSelf:'center'
    },
    rectangle1:{
        alignSelf:'center'
    },
 
    MatchPlay:{
        color:'white',
        marginLeft:'33%',
        fontSize:20
    },
    rectangle2:{
        alignSelf:"center",
        marginTop:"2%"
    },
    user:{
        height:25,
        width:25,
       resizeMode:"stretch",
       marginLeft:"5%",
       padding:10
    },
    email:{
        height:15,
        width:15,
       resizeMode:"stretch",
       marginLeft:"5%",
       padding:10
    },
    subView:{
        backgroundColor:"rgba(255,255,255,0.41)",
        borderRadius:50,
        marginHorizontal:"5%",
        flexDirection:'row',
        marginTop:"7%",
        alignItems:"center"
    },
    forgot_password:{
        color:'white',
        fontSize:16,
        marginLeft:"55%",
        marginTop:'3%'
     }
 })
 export default signup;
 