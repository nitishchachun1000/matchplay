import React from "react";
import {View,Text,Image} from 'react-native'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import {createBottomTabNavigator} from 'react-navigation-tabs'
import splashScreen from '../../App/Screens/splash'
import signInScreen from '../../App/Screens/signIn/index'
import signupScreen from '../../App/Screens/signup'
import forgotPassword from "../Screens/forgotPassword"
import otpVerify from "../Screens/otpVerify/index"
import homeScreen from '../Screens/homeScreen/index'
import Header from "../component/Header"
import setPassword from '../Screens/setPassword'
import requestMatch from '../Screens/requestMatch'
import pendingMatch from '../Screens/pendingMatch'
import bettingScreen from "../Screens/bettingScreen"
import  addScreen  from "../Screens/addScreen";
import drawerScreen from '../Screens/drawerScreen'
 

const AppStack = createStackNavigator(
    {
      splash: {
        screen: splashScreen,
        navigationOptions: {
          headerShown: false,
        },
      },

      homeScreen: {
        screen: homeScreen,
        navigationOptions: {
          headerShown: false,
        },
      },

    
      
      signIn: {
        screen: signInScreen,
        navigationOptions: {
          headerShown: false,
        },
      },
    
     signup: {
        screen: signupScreen,
        navigationOptions: {
          headerShown: false,
        },
      },

      forgotPassword: {
        screen: forgotPassword,
        navigationOptions: {
          headerShown: false,
        },
      },

      otpVerify: {
        screen: otpVerify,
        navigationOptions: {
          headerShown: false,
        },
      },

      Header: {
        screen: Header,
        navigationOptions: {
          headerShown: false,
        },
      },
      
      setPassword: {
        screen: setPassword,
        navigationOptions: {
          headerShown: false,
        },
      },

      requestMatch: {
        screen: requestMatch,
        navigationOptions: {
          headerShown: false,
        },
      },

      pendingMatch: {
        screen: pendingMatch,
        navigationOptions: {
          headerShown: false,
        },
      },

      bettingScreen: {
        screen: bettingScreen,
        navigationOptions: {
          headerShown: false,
        },
      },

      addScreen: {
        screen: addScreen,
        navigationOptions: {
          headerShown: false,
        },
      },

      drawerScreen:{
        screen:drawerScreen,
        navigationOptions:{
          headerShown:false,
        },
      },


      
    },
    {
      initialRouteName: 'splash',
    },
  );

  const tabNavigator= createBottomTabNavigator({
    requestMatch: {
      screen: requestMatch,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: "requestMatch ",
        tabBarIcon: ({ focused, tintColor }) => {
          let icon
          if (navigation.state.routeName === "requestMatch") {
            icon = focused ? require('../assets/request_green.png') :
              require('../assets/requestIcon.png')
  
          }
          let txt
          if (navigation.state.routeName === "requestMatch") {
            txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold" }}>REQUEST M.</Text> :
              <Text style={{ fontSize: 7, fontWeight: "bold" ,color:"rgba(0,0,0,0.37)"}}>REQUEST M.</Text>
  
          }
          return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}>
            <Image source={icon}
            style={{
              width:24,
              height:24, marginLeft: 7
            }}></Image>
  
            <View>
              <Text >{txt}</Text>
              </View>
          </View>
        }
      })
    },

    pendingMatch: {
      screen: pendingMatch,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: "pendingMatch ",
        tabBarIcon: ({ focused, tintColor }) => {
          let icon
          if (navigation.state.routeName === "pendingMatch") {
            icon = focused ? require('../assets/pending_green.png') :
              require('../assets/pendingIcon.png')
  
          }
          let txt
          if (navigation.state.routeName === "pendingMatch") {
            txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold" }}>PENDING M.</Text> :
              <Text style={{ fontSize: 7, fontWeight: "bold" ,color:"rgba(0,0,0,0.37)"}}>PENDING M.</Text>
  
          }
          return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}>
            <Image source={icon}
            style={{
              width:24,
              height:24, marginLeft: 7
            }}></Image>
  
            <View>
              <Text >{txt}</Text>
              </View>
          </View>
        }
      })
    },

    addScreen: {
      screen: addScreen,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: "pendingMatch ",
        tabBarIcon: ({ focused, tintColor }) => {
          let icon
          if (navigation.state.routeName === "addScreen") {
            icon = focused ? require('../assets/addIcon.png') :
              require('../assets/addIcon_green.png')
  
          }
          let txt
          if (navigation.state.routeName === "addScreen") {
            txt = focused ? <Text style={{ color: "green", fontSize: 10, fontWeight: "bold" }}>ADD</Text> :
              <Text style={{ fontSize: 10, fontWeight: "bold" ,color:"rgba(0,0,0,0.37)"}}>ADD</Text>
  
          }
          return <View style={{ height:33,width:33,borderRadius:33,borderColor:"green", marginLeft: 10, marginBottom: 50, }}>
            <Image source={icon}
            style={{
              width:30,
              height:30, marginLeft: 7
            }}></Image>
  
            <View>
              <Text style={{marginLeft:11}}>{txt}</Text>
              </View>
          </View>
        }
      })
    },

    bettingScreen: {
      screen: bettingScreen,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: "bettingScreen ",
        tabBarIcon: ({ focused, tintColor }) => {
          let icon
          if (navigation.state.routeName === "bettingScreen") {
            icon = focused ? require('../assets/betting_green.png') :
              require('../assets/betting.png')
  
          }
          let txt
          if (navigation.state.routeName === "bettingScreen") {
            txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold" }}>BETTING</Text> :
              <Text style={{ fontSize: 7, fontWeight: "bold" ,color:"rgba(0,0,0,0.37)"}}>bETTING</Text>
  
          }
          return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}>
            <Image source={icon}
            style={{
              width:24,
              height:24, marginLeft: 7
            }}></Image>
  
            <View>
              <Text >{txt}</Text>
              </View>
          </View>
        }
      })
    },

    homeScreen: {
      screen: homeScreen,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: "bettingScreen ",
        tabBarIcon: ({ focused, tintColor }) => {
          let icon
          if (navigation.state.routeName === "homeScreen") {
            icon = focused ? require('../assets/feed_green.png') :
              require('../assets/feed.png')
  
          }
          let txt
          if (navigation.state.routeName === "homeScreen") {
            txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold" }}>FEED</Text> :
              <Text style={{ fontSize: 7, fontWeight: "bold" ,color:"rgba(0,0,0,0.37)"}}>FEED</Text>
  
          }
          return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}>
            <Image source={icon}
            style={{
              width:24,
              height:24, marginLeft: 7
            }}></Image>
  
            <View>
              <Text style={{marginLeft:10}}>{txt}</Text>
              </View>
          </View>
        }
      })
    },
    
    },
    {
      initialRouteName: "homeScreen",
      tabBarPosition: 'bottom',
      swipeEnabled: true,
      tabBarOptions: {
        activeTintColor: 'white',
        activeBackgroundColor: 'white',
        inactiveTintColor: 'white',
        inactiveBackgroundColor: '#FFF',
        showIcon: true,
        showLabel: true,
        style: { height:50, borderTopColor: 'white', borderTopWidth: 1, elevation: 20,backgroundColor:'red' },
        labelStyle: {
          fontSize: 1,
          fontWeight: "bold",
    
        }
      }
    }
    ),

    drawerNavigator =  createDrawerNavigator({
      homeScreen:{
        screen:homeScreen
      },
     
  },
  {
    contentOptions: {
        style: {
          backgroundColor: "white",
          flex: 1,
        }
      },
      navigationOptions: {
        drawerLockMode: 'locked-closed',
      },
      contentComponent: drawerScreen, 
    },
  
  )

    Routes = createAppContainer(
    createSwitchNavigator({
     app:AppStack,
     tabNavigator2:tabNavigator,
     drawer:drawerNavigator
    }),
  );
  export default Routes;