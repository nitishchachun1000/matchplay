import React, { Component } from 'react';
import Routes from './App/Navigation/index'
export default class App extends Component {
  render() {
    return (
      <Routes />
    );
  }
}

// import React, {useState, useRef} from 'react';
// import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
// import Video from 'react-native-video';
// import MediaControls, {PLAYER_STATES} from 'react-native-media-controls';

// const App = () => {
//   const videoPlayer = useRef(null);
//   const [currentTime, setCurrentTime] = useState(0);
//   const [duration, setDuration] = useState(0);
//   const [isFullScreen, setIsFullScreen] = useState(false);
//   const [isLoading, setIsLoading] = useState(true);
//   const [paused, setPaused] = useState(false);
//   const [ playerState, setPlayerState] = useState(PLAYER_STATES.PLAYING);
//   const [screenType, setScreenType] = useState('content');

//   const onSeek = (seek) => {
//     videoPlayer.current.seek(seek);
//   };

//   const onPaused = (playerState) => {
//     setPaused(!paused);
//     setPlayerState(playerState);
//   };

//   const onReplay = () => {
//     setPlayerState(PLAYER_STATES.PLAYING);
//     videoPlayer.current.seek(0);
//   };

//   const onProgress = (data) => {
//     if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
//       setCurrentTime(data.currentTime);
//     }
//   };

//   const onLoad = (data) => {
//     setDuration(data.duration);
//     setIsLoading(false);
//   };

//   const onLoadStart = (data) => setIsLoading(true);

//   const onEnd = () => setPlayerState(PLAYER_STATES.ENDED);

//   const onError = () => alert('Oh! ', error);

//   const exitFullScreen = () => {
//     alert('Exit full screen');
//   };

//   const enterFullScreen = () => {};

//   const onFullScreen = () => {
//     setIsFullScreen(isFullScreen);
//     if (screenType == 'content') setScreenType('cover');
//     else setScreenType('content');
//   };

//   const renderToolbar = () => (
//     <View>
//       <Text style={styles.toolbar}> toolbar </Text>
//     </View>
//   );

//   const onSeeking = (currentTime) => setCurrentTime(currentTime);

//   const changeRate=()=>{
//     <View style={styles.subView}>
//       <Text style={{color:"green"}}>0.75X</Text>
//       <Text>1X</Text>
//       <Text>1.5X</Text>
//       <Text>2X</Text>
//       </View>
//   }

//   return (
//     <View style={{flex: 1}}>
//       <Video
//       onPlaybackRateChange={changeRate}
//         onEnd={onEnd}
//         muted={true}
//         playInBackground={true}
//         onLoad={onLoad}
//         onLoadStart={onLoadStart}
//         onProgress={onProgress}
//         paused={paused}
//         ref={videoPlayer}
//         resizeMode={screenType}
//         onFullScreen={isFullScreen}
//         source={require("../project1/App/assets/video.mp4")}
//         style={styles.mediaPlayer}
//         volume={10}
//       />
//       <MediaControls
//         duration={duration}
//         isLoading={isLoading}
//         mainColor="green"
//         onFullScreen={onFullScreen}
//         onPaused={onPaused}
//         onReplay={onReplay}
//         onSeek={onSeek}
//         onSeeking={onSeeking}
//         playerState={playerState}
//         progress={currentTime}
//         toolbar={renderToolbar()}
//       />
//     </View>
//   );
// };

// export default App;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   toolbar: {
//     marginTop: 30,
//     backgroundColor: 'white',
//     padding: 10,
//     borderRadius: 5,
//   },
//   mediaPlayer: {
//     position: 'absolute',
//    height:"40%",
//    width:"100%",
//    marginTop:"50%",
//     backgroundColor: 'white',
//     justifyContent: 'center',
//     alignItems:"center"
//   },
//   subView:{
//   flexDirection:"row",
//   justifyContent:"space-evenly",
//   marginTop:"10%"
//   }
// });